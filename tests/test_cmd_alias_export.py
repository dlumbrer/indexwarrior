#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright (C) 2018 Bitergia
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#
# Authors:
#     Luis Cañas-Díaz <lcanas@bitergia.com>
#

import httpretty
import io
import json
import os
import sys
import unittest
import unittest.mock as mock

from indexwarrior.main import IndexWarrior
from operator import itemgetter

if '..' not in sys.path:
    sys.path.insert(0, '..')


def read_file(filename, mode='r'):
    """Adhoc function to make reading files easier"""
    with open(os.path.join(os.path.dirname(os.path.abspath(__file__)), filename), mode) as fdesc:
        content = fdesc.read()
        return content


class TestCommandAlias(unittest.TestCase):
    """Test alias command"""

    @httpretty.activate
    @unittest.mock.patch('elasticsearch.Elasticsearch.ping')
    def test_alias_export(self, mock_connect):
        """ Test subcommand alias export """
        body_cat_indices = read_file('data/cat_indices.json')
        body_cat_aliases = read_file('data/cat_aliases.json')
        body_github_aliases = read_file('data/github-index_aliases.json')
        body_git_aliases = read_file('data/git-index_aliases.json')
        body_pipermail_aliases = read_file('data/pipermail-index_aliases.json')
        valid_command_output = read_file('data/output_aliases_with_filters_exported.json')

        expected_dict = json.loads(valid_command_output)

        httpretty.register_uri(httpretty.GET,
                               "http://iwarrior.biterg.io:9200/_cat/indices?format=json",
                               body=body_cat_indices,
                               content_type="application/json")

        httpretty.register_uri(httpretty.GET,
                               "http://iwarrior.biterg.io:9200/_cat/aliases?format=json",
                               body=body_cat_aliases,
                               content_type="application/json")

        httpretty.register_uri(httpretty.GET,
                               "http://iwarrior.biterg.io:9200/github_grimoirelab_170908_enriched_170908b/_alias",
                               body=body_github_aliases,
                               content_type="application/json")

        httpretty.register_uri(httpretty.GET,
                               "http://iwarrior.biterg.io:9200/git_grimoirelab_171229_enriched_171229/_alias",
                               body=body_git_aliases,
                               content_type="application/json")

        httpretty.register_uri(httpretty.GET,
                               "http://iwarrior.biterg.io:9200/pipermail_grimoirelab_170908__enriched_170908b/_alias",
                               body=body_pipermail_aliases,
                               content_type="application/json")


        mock_connect.return_value = True
        iwarrior = IndexWarrior("http://iwarrior.biterg.io", timeout=30)
        iwarrior.initialize()
        captured_output = io.StringIO()                 # Create StringIO object
        sys.stdout = captured_output                    #  and redirect stdout.
        iwarrior.export_aliases()                       # Call function.
        sys.stdout = sys.__stdout__                     # Reset redirect
        output_dict = json.loads(captured_output.getvalue())

        # it is not possible to compare the whole dictionaries as they have a inner and single
        # list which contains all the data. Then we have to extract the elements inside the
        # list and the compare them

        expected_list = []
        for a in expected_dict['actions']:
            expected_list.append(a['add'])
        expected_list = sorted(expected_list, key=itemgetter('index','alias'))

        output_list = []
        for t in output_dict['actions']:
            output_list.append(t['add'])
        output_list = sorted(output_list, key=itemgetter('index','alias'))

        self.assertListEqual(expected_list, output_list)
