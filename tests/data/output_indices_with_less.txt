+-------------+------------------------------------------------+-------+------------+--------------+-----------------------+
| status      | index                                          | alias | docs.count | docs.deleted | rep/pri size/pri.size |
+-------------+------------------------------------------------+-------+------------+--------------+-----------------------+
| yellow/open | .kibana                                        |       | 197        | 5            | 1/1 333.8kb/333.8kb   |
| yellow/open | git_grimoirelab_170908_enriched_170908         |       | 23426      | 1011         | 1/5 50.4mb/50.4mb     |
| yellow/open | git_grimoirelab_170908_enriched_170908b        |       | 23953      | 4216         | 1/5 63.6mb/63.6mb     |
| yellow/open | git_grimoirelab_171116_enriched_171116         |       | 26078      | 2622         | 1/5 65.6mb/65.6mb     |
| yellow/open | git_grimoirelab_171229_enriched_171229         | *     | 26147      | 3285         | 1/5 73.3mb/73.3mb     |
| yellow/open | github                                         |       | 29         | 0            | 1/5 27.6kb/27.6kb     |
| yellow/open | github_grimoirelab_170908_enriched_170908      |       | 495        | 14           | 1/5 1.5mb/1.5mb       |
| yellow/open | github_grimoirelab_170908_enriched_170908b     | *     | 916        | 23           | 1/5 2.4mb/2.4mb       |
| yellow/open | pipermail_grimoirelab_170908__enriched_170908  |       | 6          | 1            | 1/5 109.3kb/109.3kb   |
| yellow/open | pipermail_grimoirelab_170908__enriched_170908b | *     | 44         | 4            | 1/5 534kb/534kb       |
+-------------+------------------------------------------------+-------+------------+--------------+-----------------------+
